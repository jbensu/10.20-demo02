import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {//定义变量
    tasklist: [],
    // count: 0
  },
  getters: {//计算属性和computed用法差不多
    unFinishCount(state) {
      var arr1 = state.tasklist.filter((item) => {
        return item.status == false;//过滤当前数组，留下状态为false的
      })
      return arr1.length;
    }
  },
  mutations: {//同步代码
    addtesk(state, payload) {
      var obj = { id: state.tasklist.length + 1, name: payload, status: false }
      state.tasklist.push(obj);
    },
    changeStatus(state, payload) {
      //查找需要修改状态的那个任务对象
      var fi = state.tasklist.find((item) => {
        return item.id == payload.id
      });

      if (fi) { //判断是否找到
        fi.status = !fi.status;
      }
    }


  },
  actions: {//异步代码
  },
  modules: {
  }
})





